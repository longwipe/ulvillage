# OCB MicroSplat Shader Mod  - 7 Days to Die (A20) Addon

Replacing vanilla terrain MicroSplat shader with an improved version.

You need to disable EAC to use this mod!

## Description

It is still the same base [MicroSplat][4] shader, but I've added e.g.
the anti-tiling module for better visual fidelity. It will also
allow me to open up the shader to use 32 textures instead of 24.
Currently 3 different performance/quality settings are included,
which are automatically switched with the in-game terrain quality.

## Download

End-Users are encouraged to download my mods from [NexusMods][3].  
Every download there helps me to buy stuff for mod development.

Otherwise please use one of the [official releases][2] here.  
Only clone or download the repo if you know what you do!

## Road-Map

In the future this mod will also enable:
- To replace/add MicroSplat terrain textures
- To add 3 more main Biomes for a total of 8
- To add at least 8 more distinct (ore) blocks

Some features that may be included at some point:
- Replacing current 2k with 4k terrain textures

## Changelog

### Version 0.1.2

- Fix issue when changing options without game loaded

### Version 0.1.1

- Fix normals and noise

### Version 0.1.0

- Initial version

## Compatibility

I've developed and tested this Mod against version a20.7(b1).

[1]: https://github.com/OCB7D2D/OcbMicroSplat
[2]: https://github.com/OCB7D2D/OcbMicroSplat/releases
[3]: https://www.nexusmods.com/7daystodie/mods/2873
[4]: https://assetstore.unity.com/packages/tools/terrain/microsplat-96478