# OCB Electricity Solar Recipes Mod - 7 Days to Die Addon

This (XML-Only) Modlets adds two recipes to make solar panels
and cells craftable within the workbench. Both items are locked
behind their schematics/blueprints.
