OVERVIEW:
Requires Undead Legacy 2.6.17 which you can check out and download here: https://ul.subquake.com/

Makes the zombie melee range realistic in Undead Legacy. No more bs hits from a cars length away.

MOD DESCRIPTION:
Makes the melee range the same as yours is for all UL zombies.

INSTALL:
Simply drag the folder into steamapps/common/7Days/Mods where all of the other Undead Legacy files will be. The folder name starts with "Z" to load after Undead Legacy's file order.

Just the server files or host requires this to be installed.

COPYRIGHT & DISCLAIMER:
This mod isn't affiliated with Undead Legacy in its original form. It's simply an add-on to make it that bit more enjoyable for like minded people.

SUGGESTIONS, FEEDBACK & COMMUNITY:
If you ever want to play Undead Legacy with our small group, discuss future modlets to improve, or just tell me how much this mod sucks feel free to join our Discord: https://discord.gg/nRunn9XQTS

MY OTHER UNDEAD LEGACY MODS:
https://www.nexusmods.com/users/15826559?tab=user+files

CHANGELOG:
v1.0
- Mod created.